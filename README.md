CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Maintainers
 
Introduction
------------

This module integrates Semantic UI form into Drupal.

Requirements
------------

Semantic UI library  
https://semantic-ui.com/

Installation
------------

- Download the Semantic UI library  
- Extract to your libraries directory as:  
``libraries/semantic``  
- Enable module as other Drupal module

Maintainers
-----------

 * Lap Pham (phthlaap) - https://www.drupal.org/user/3579545
 * Nhan Nguyen (nhan.nguyenthanh@kyanon.digital)
