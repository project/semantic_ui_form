/**
 * @file
 * semantic.form.integration.js
 *
 * Defines the behaviors of semantic form validation.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';
  $.fn.form.settings.selector.group = '.js-form-item';
  $.fn.form.settings.rules.validateCheckboxesRequired = function () {
    var checkboxes = $(this).parents('.checkboxes-js-form-required');
    var input = checkboxes.find('input[type="checkbox"]');
    var validated = false;
    input.each(function () {
      if ($(this).prop("checked") == true) {
        validated = true;
      }
    });
    if (validated == true) {
      checkboxes.find('.error-checkboxes').remove();
      checkboxes.removeClass('error');
      checkboxes.find('.prompt').remove();
    }
    return validated;
  };
  var semantic = {};
  semantic.fieldSelector = 'input[type="tel"], input[type="email"], input[type="text"], input[type="number"], input[type="radio"], select, textarea';
  semantic.checkboxesPrompt = function (value) {
    var checkboxes = $('input[value="' + value + '"]').parents('.checkboxes-js-form-required');
    checkboxes.find('.error-checkboxes').remove();
    checkboxes.addClass('error');
    checkboxes.append('<div class="error-checkboxes ui basic red pointing prompt label">Complete this field.</div>');
    return 'Complete this field.';
  };

  semantic.buildValidateFields = function ($form) {
    var $fields = $form.find(semantic.fieldSelector);
    var validateFields = {};
    $fields.each(function () {
      var field_name = $(this).attr('name');
      validateFields[field_name] = {
        identifier: field_name,
        rules: semantic.buildFieldRules($(this))
      };
    });
    $form.find('.checkboxes-js-form-required').each(function () {
      var checkboxes = $(this);
      var input = checkboxes.find('input[type="checkbox"]');
      console.log(input);
      input.each(function () {
        var field_name = $(this).attr('name');
        validateFields[field_name] = {
          identifier: field_name,
          rules: [{
            type: 'validateCheckboxesRequired',
            prompt: semantic.checkboxesPrompt,
          }],
        };
      });
    });
    return validateFields;
  };
  semantic.buildFieldRules = function ($field) {
    var rules = [];
    var required = $($field).attr('required');
    if (required === 'required' || required === true) {
      rules.push({
        type: 'empty',
        prompt: 'Complete this field.'
      });
      if ($field.prop('tagName') === 'SELECT') {
        rules.push({
          type: 'not[_none]',
          // prompt: '{name} must have a value',
          prompt: '--Please select one--',
        });
      }
    }

    var minlength = $($field).attr('minlength');
    if (minlength) {
      rules.push({type: 'minLength[' + minlength + ']'});
    }

    return rules;
  };
  semantic.scrollToError = function () {
    var $promptElement = $('.ui.basic.red')
      .parent()
      .get(0);
    $promptElement.scrollIntoView();
  };
  Drupal.semantic = semantic;

  Drupal.behaviors.semanticFormIntegraion = {
    attach: function (context) {
      var $forms = $('form');
      $forms.each(function () {
        $(this).form({
          keyboardShortcuts: false,
          inline: true,
          fields: Drupal.semantic.buildValidateFields($(this)),
          onFailure: function () {
            Drupal.semantic.scrollToError();
            return false;
          },
        });

        $(this)
          .find('.form-item--error-message')
          .attr('class', 'ui basic red pointing prompt label');
      });
    }
  };

}(jQuery, Drupal, drupalSettings));
