<?php

namespace Drupal\semantic_ui_form\Form;

use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Theme\ThemeManagerInterface;

/**
 * Class SemanticFormConfig.
 */
class SemanticFormConfig extends ConfigFormBase {

  /**
   * Drupal\Core\Theme\ThemeManagerInterface definition.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  protected $themeHandler;

  /**
   * Constructs a new SemanticFormConfig object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ThemeManagerInterface $theme_manager,
    ThemeHandlerInterface $themeHandler
  ) {
    parent::__construct($config_factory);
    $this->themeManager = $theme_manager;
    $this->themeHandler = $themeHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('theme.manager'),
      $container->get('theme_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'semantic_ui_form.semanticformconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'semantic_form_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('semantic_ui_form.semanticformconfig');
    $form = parent::buildForm($form, $form_state);
    $selected_themes = $config->get('theme');
    $themes = $this->themeHandler->listInfo();
    $options = [];
    foreach ($themes as $key => $item) {
      $options[$key] = $item->getName();
    }

    $form['theme'] = [
      '#title' => $this->t('Themes:'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $selected_themes,
      '#description' => $this->t('Select the theme you want to enable Semantic UI Form.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('semantic_ui_form.semanticformconfig');
    $themes = $form_state->getValue('theme');
    $config_theme = [];
    foreach ($themes as $key => $value) {
      if ($value != '0') {
        $config_theme[] = $key;
      }
    }
    $config->set('theme', $config_theme);
    $config->save();
  }

}
